FROM alpine:3.10

MAINTAINER Noah <noahgray@me.com>

RUN apk add android-tools --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing

RUN apk add --update --no-cache python python-dev gcc musl-dev curl 

# Compile CRC32c
# See for more information: https://cloud.google.com/storage/docs/gsutil/addlhelp/CRC32CandInstallingcrcmod
#
RUN curl -L -o crcmod.tar.gz "https://downloads.sourceforge.net/project/crcmod/crcmod/crcmod-1.7/crcmod-1.7.tar.gz" && \
    tar -xzf crcmod.tar.gz && \
    cd crcmod-1.7/ && \
    python setup.py install && \
    cd ..

RUN apk update && apk upgrade && \
  apk add --no-cache \
    openjdk8 \
    autoconf \
    automake \
    jq \
    nodejs \
    npm \
    yarn \
    alpine-sdk \
    python2 \
    libexecinfo-dev \
    openssh \
    keychain \
    bash

ENV JAVA_HOME="/usr/lib/jvm/java-1.8-openjdk"

RUN yarn global add detox-cli
RUN yarn global add node-gyp

RUN wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN mkdir /android-sdk
RUN unzip sdk-tools-linux-4333796.zip -d /android-sdk
RUN rm sdk-tools-linux-4333796.zip
ENV ANDROID_HOME="/android-sdk"

# Install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

# Install gcloud
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-238.0.0-linux-x86_64.tar.gz
RUN tar zxvf google-cloud-sdk-238.0.0-linux-x86_64.tar.gz google-cloud-sdk
ENV PATH /google-cloud-sdk/bin:$PATH
RUN rm google-cloud-sdk-238.0.0-linux-x86_64.tar.gz

ENV LC_ALL=en_US.UTF-8
