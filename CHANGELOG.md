# Changelog

## Fri Feb 21 09:56:03 2020 PST

Adding OpenSSH and keychain packages for pushing to Gitlab and other places.

See docs: https://www.funtoo.org/Keychain

Bump to v0.0.4

## Sat Nov  9 23:41:31 2019 PST

Some kind of error suggested this bug re-emerged in Apline/gcloud: (StackOverflow)[https://stackoverflow.com/questions/44156905/install-crcmod-crc32c-c-extension-on-alpine/44191400%5C#44191400]

https://gitlab.alpinelinux.org/alpine/aports/issues/7343

It's an old issue but seems related to errors we're getting.